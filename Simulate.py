from matplotlib.collections import PatchCollection
from matplotlib.animation import ArtistAnimation as anim
import numpy as np
import matplotlib.pyplot as plt
from Functions import init, sim


# Time duration and step size
Time = 100
dt = 0.1
# Number of particles. Has to be multiple of 10
N = 200

# Dimensionless scaling parameters
L_s = 0.08
L_a = 0.2
L_n = 0.03
L_Fin = 0.3
L_Tin = 3
## Forces and torques
#Fs = L_s
#Fin = L_Fin
#Ta = L_a
#Tn = np.sqrt(L_n/dt)
#Tin = L_Tin

## For animation
#collection = []
#fig, ax = plotPrt(prt)

# Put particles in rectangle with square latice
# and orient them along the long axis if wanted
initialise = input("Reset? Y/N        ")
if initialise=="Y" or initialise=="y":
    prt = init(N)
    fig, ax = plt.subplots()
else:
    ANM.event_source.stop()
    for i in range(len(collection)):
        for j in range(len(collection[-1])):
            collection[i][j].set_visible(False)
    collection0 = collection
    rect0 = rect
    
prt, collection, rect = sim(prt, dt, [L_s, L_a, L_n, L_Fin, L_Tin], Time, 1.0, fig, ax)

if initialise!="Y" and initialise!="y":
    collection0 += collection[1:]
    collection = collection0
    rect[0] = min(rect[0], rect0[0])
    rect[1] = max(rect[1], rect0[1])
    rect[2] = min(rect[2], rect0[2])
    rect[3] = max(rect[3], rect0[3])

np.save('prt', prt)
np.save('rect', rect)
np.save('collection', collection)

# Animate particles with orientations
#fig, ax = plt.subplots()
#ax.add_collection(PatchCollection(collection[-1], edgecolor='none', alpha=0.5))
#ax.autoscale_view()
plt.xlim(rect[0]-2, rect[1]+2)
plt.ylim(rect[2]-2, rect[3]+2)
for i in range(len(collection)):
    for j in range(len(collection[-1])):
        collection[i][j].set_visible(True)
        
ANM = anim(fig, collection, repeat=True, interval=50)
#plt.show()
