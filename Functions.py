import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
#from matplotlib.patches import Circle
#from matplotlib.patches import Arrow
from matplotlib.collections import PatchCollection


def unitvec(angle):
# Create unit vector pointing in direction with angle
    return np.array([np.cos(angle), np.sin(angle)])


def plotPrt(prt):
    phi = norm([ sum(np.cos(prt[:,2])), sum(np.sin(prt[:,2])) ]) / len(prt)
    fig, ax = plt.subplots()
    patches = [ax.add_artist(plt.Circle((x_, y_), s_, alpha=0.5)) for x_, y_, s_ in np.broadcast(prt[:,0], prt[:,1], prt[:,3])]
    patches += [ax.add_artist(plt.Arrow(x_, y_, s_, t_)) for x_, y_, s_, t_ in np.broadcast(prt[:,0], prt[:,1], np.cos(prt[:,2])*prt[:,3], np.sin(prt[:,2])*prt[:,3])]
    patches += [ax.text(0.3,1.05, 'phi='+str(phi), transform=ax.transAxes)]
#    ax.add_collection(PatchCollection(patches, edgecolor='none', alpha=0.5))
    plt.xlim(min(prt[:,0])-2, max(prt[:,0])+2)
    plt.ylim(min(prt[:,1])-2, max(prt[:,1])+2)
    return fig, ax


def zoom(prt, n, R):
    plt.xlim(prt[n,0]-R,prt[n,0]+R)
    plt.ylim(prt[n,1]-R,prt[n,1]+R)


def init(N):
    # Array of particle info: x, y, angle, radius
    prt = np.zeros([N,4])
    # Initialise as in paper
    prt[:,3] = np.random.randn(N)*0.1+1;
    prt[:,2] = np.random.randn(N)*0.1*np.pi/4
    for m in range(int (N/10)):
        for n in range(10):
            prt[10*m+n,0] = -(N/10-1) + m*2
            prt[10*m+n,1] = -9 + n*2
    return prt


def step(prt, dt, L):
# Calculate new particle positions
    
    N = len(prt)
    
    # Dimensionless scaling parameters
    L_s = L[0]
    L_a = L[1]
    L_n = L[2]
    L_Fin = L[3]
    L_Tin = L[4]
    # Forces and torques
    Fs = L_s
    Fin = L_Fin
    Ta = L_a
    Tn = np.sqrt(L_n/dt)
    Tin = L_Tin
    
    # Array of neighbours per particle
    nb = [[] for n in range(N)]
    # Array of forces per particle
    F = np.zeros([N,2])
    T = np.zeros(N)
    
    # Find neighbours, save angles and add repulsive force
    for p in range(N):
        for q in range(p+1,N):
            rp = prt[p,0:2]
            rq = prt[q,0:2]
            dX = rp - rq   #distance vector
            dis = norm(dX)   #distance
            if dis < 2.7:
                angle = np.arctan(dX[1]/dX[0]) + (np.pi if -dX[0]<0 else 0)
                nb[p].append(angle)
                nb[q].append(angle+np.pi)
                d = prt[p,3]+prt[q,3]-dis
                Frp = d*dX/dis if d>0 else 0
                F[p,:] += Frp
                F[q,:] -= Frp
                # Add alignment torque
                dpsi = ( prt[q][2]-prt[p][2] )%(np.pi)
                T[p] += Ta*dpsi;
                T[q] += Ta*-dpsi;
    
    # Add forces for self-propulsion and boundary
    # and torques for noise and boundary
    for n in range(N):
        # Add self-propulsion
        FF = Fs;
        # Add noise torque
        T[n] += (np.random.rand()-0.5)*2 *Tn
        # Add boundary force and torque
        nb[n].sort()
        L = len(nb[n]);
        for m in range(L):
            theta_out = ( nb[n][(m+1)%L]-nb[n][m] )%(2*np.pi)
            if theta_out>np.pi:
                # Add boundary force
                FF += (theta_out-np.pi)*Fin
                # Add boundary torque
#                theta1 = nb[n][m]
#                theta2 = nb[n][(m+1)%L]
                theta_in = nb[n][(m+1)%L]+(2*np.pi-theta_out)/2
                theta_in = theta_in%(2*np.pi)
                dtheta = ( theta_in-prt[n][2] +np.pi)%(2*np.pi) -np.pi
                T[n] += Tin*dtheta
        F[n] += FF*unitvec(prt[n][2])
        
    prt[:,0] += F[:,0]/prt[:,3]*dt
    prt[:,1] += F[:,1]/prt[:,3]*dt
    prt[:,2] += T/prt[:,3]**2*dt
    
    return prt


def sim(prt, dt, L, simTime, frameTime, fig, ax):
    rect = [0, 0, 0, 0]
    Nit = np.ceil(simTime/dt)
    frameStep = np.floor(frameTime/dt)
    Nfr = np.ceil(Nit/frameStep)
    Nit = int(Nfr*frameStep)
    print(Nit)
    print(Nfr)
    
    collection = []
    phi = norm([ sum(np.cos(prt[:,2])), sum(np.sin(prt[:,2])) ]) / len(prt)
    patches = [ax.add_artist(plt.Circle((x_, y_), s_, alpha=0.5)) for x_, y_, s_ in np.broadcast(prt[:,0], prt[:,1], prt[:,3])]
    patches += [ax.add_artist(plt.Arrow(x_, y_, s_, t_)) for x_, y_, s_, t_ in np.broadcast(prt[:,0], prt[:,1], np.cos(prt[:,2])*prt[:,3], np.sin(prt[:,2])*prt[:,3])]
    patches += [ax.text(0.3,1.05,'phi='+str(phi), transform=ax.transAxes)]
    collection += [patches]
    
    for i in range(Nit):
        print(i)
        prt = step(prt, dt, L)
        phi = norm([ sum(np.cos(prt[:,2])), sum(np.sin(prt[:,2])) ]) / len(prt)
        plt
        if (i+1)%frameStep==0:
#            plt.cla()
            for i in range(len(collection[-1])):
                collection[-1][i].set_visible(False)
            patches = [ax.add_artist(plt.Circle((x_, y_), s_, alpha=0.5)) for x_, y_, s_ in np.broadcast(prt[:,0], prt[:,1], prt[:,3])]
            patches += [ax.add_artist(plt.Arrow(x_, y_, s_, t_)) for x_, y_, s_, t_ in np.broadcast(prt[:,0], prt[:,1], np.cos(prt[:,2])*prt[:,3], np.sin(prt[:,2])*prt[:,3])]
            patches += [ax.text(0.3,1.05,'phi='+str(phi), transform=ax.transAxes)]
            collection += [patches]
            rect[0] = min(rect[0], min(prt[:,0]))
            rect[1] = max(rect[1], max(prt[:,0]))
            rect[2] = min(rect[2], min(prt[:,1]))
            rect[3] = max(rect[3], max(prt[:,1]))
            plt.xlim(rect[0]-2, rect[1]+2)
            plt.ylim(rect[2]-2, rect[3]+2)
            plt.pause(0.05)
    return prt, collection, rect


